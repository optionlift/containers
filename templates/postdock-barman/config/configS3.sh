#!/bin/sh -xe

#
# main entry point to run s3cmd
#
S3CMD_PATH=/opt/s3cmd/s3cmd

#
# Check for required parameters
#
if [ -z "${aws_key}" ]; then
    echo "The environment variable key is not set. Attempting to create empty creds file to use role."
    aws_key=""
fi

if [ -z "${aws_secret}" ]; then
    echo "The environment variable secret is not set."
    aws_secret=""
    security_token=""
fi


#
# Replace key and secret in the /.s3cfg file with the one the user provided
#
echo "" >> /.s3cfg
echo "access_key = ${aws_key}" >> /.s3cfg
echo "secret_key = ${aws_secret}" >> /.s3cfg

if [ -z "${security_token}" ]; then
    echo "security_token = ${aws_security_token}" >> /.s3cfg
fi

#
# Add region base host if it exist in the env vars
#
if [ "${s3_host_base}" != "" ]; then
  sed -i "s/host_base = s3.amazonaws.com/# host_base = s3.amazonaws.com/g" /.s3cfg
  echo "host_base = ${s3_host_base}" >> /.s3cfg
fi


# Copy file over to the default location where S3cmd is looking for the config file
cp /.s3cfg /root/

#
# Finished operations
#
echo "Finished s3cmd operations"
echo "Starting original entrypoint"
eval $1
