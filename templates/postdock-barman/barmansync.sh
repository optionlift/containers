#!/bin/bash
set -x # To debug the script
set -e # Enables checking of all commands. To execute it on production.

# Syncronizes barman backups and WALs into S3
# Usage: barmansync.sh /var/log/nagiosbarmansync.log /var/log/barmansync.log

NAGIOSLOG="/var/log/nagiosbarmansync.log"
LOG="/var/log/barmansync.log"
> $NAGIOSLOG	
BUCKETNAME="barman-backups"
DATETIME=`date +"%Y-%m-%d %T"`
echo "${DATETIME} Starting Barman sync" >> $LOG
LASTBASEBACKUP=`barman list-backup pg_cluster | grep -v failed | head -1 | cut -d ' ' -f2`
barman list-files pg_cluster $LASTBASEBACKUP --target full > /tmp/exp.$LASTBASEBACKUP
s3cmd sync -p /tmp/exp.$LASTBASEBACKUP s3://${BUCKETNAME}/barman/${LASTBASEBACKUP}/ >> $LOG
if [ $? -eq 0 ]; then
echo "${DATETIME} Barman export sync OK" >> $NAGIOSLOG
else
echo "${DATETIME} Barman export sync FAIL" >> $NAGIOSLOG
fi
s3cmd sync -p /var/backups s3://${BUCKETNAME}/barman/${LASTBASEBACKUP}/ >>$LOG
if [ $? -eq 0 ]; then
echo "${DATETIME} Barman sync OK" >> $NAGIOSLOG
else
echo "${DATETIME} Barman sync FAIL" >> $NAGIOSLOG
fi
DATETIME=`date +"%Y-%m-%d %T"`
echo "${DATETIME} Finishing Barman sync" >> $LOG
echo "******************************************" >> $LOG
echo " " >> $LOG
