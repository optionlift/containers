#!/bin/bash
IMAGE=postdock-barman
ID=$(docker build  -t ${IMAGE}  .  | tail -1 | sed 's/.*Successfully built \(.*\)$/\1/')
echo $ID
docker tag ${ID} registry.registry.stage.300bs.online/${IMAGE}:2.3
docker push registry.registry.stage.300bs.online/${IMAGE}:2.3