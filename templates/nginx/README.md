[![Docker Repository on Quay](https://quay.io/repository/optionlift/nginx/status "Docker Repository on Quay")](https://quay.io/repository/optionlift/nginx)

### Environment variables

NGINX_PORT - (default 80, e.g. 8080)

NGINX_SERVER -  (default default.local)

NGINX_FASTCGI_PASS - (default php:9000)