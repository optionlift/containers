[![Docker Repository on Quay](https://quay.io/repository/optionlift/nginx/status "Docker Repository on Quay")](https://quay.io/repository/optionlift/clickhouse)

### Repository

https://hub.docker.com/r/yandex/clickhouse-server/

### Configuration

Container exposes 8123 port for HTTP interface and 9000 port for native client).
ClickHouse configuration represented with a file "config.xml" (documentation)

#### Start server instance with custom configuration
$ docker run -d --name some-clickhouse-server --ulimit nofile=262144:262144 -v /path/to/your/config.xml:/etc/clickhouse-server/config.xml yandex/clickhouse-server